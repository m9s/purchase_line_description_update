#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Purchase Line Description Update',
    'name_de_DE': 'Einkaufsposition Aktualisierung Beschreibung',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''
    - Updates the description of a purchase  line on change of a product
''',
    'description_de_DE': '''
    - Aktualisiert die Beschreibung einer Einkaufsposition, wenn das Produkt
    einer Position geändert wird.
''',
    'depends': [
        'purchase',
    ],
    'xml': [
    ],
}
